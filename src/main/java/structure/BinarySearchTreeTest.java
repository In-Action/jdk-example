package structure;

import org.junit.Test;

/**
 * @author he peng
 * @create 2018/4/18 18:06
 * @see
 */
public class BinarySearchTreeTest {

    @Test
    public void putVal() throws Exception {
        BinarySearchTree1 binarySearchTree = new BinarySearchTree1();
        binarySearchTree.putVal(12);
        binarySearchTree.putVal(15);
        binarySearchTree.putVal(12);
        binarySearchTree.putVal(224);
        binarySearchTree.putVal(-348);
        binarySearchTree.putVal(22);
        binarySearchTree.putVal(233);

    }

    @Test
    public void removeMin() throws Exception {
        BinarySearchTree1 binarySearchTree = new BinarySearchTree1();
        binarySearchTree.putVal(12);
        binarySearchTree.putVal(15);
        binarySearchTree.putVal(224);
        binarySearchTree.putVal(-348);
        binarySearchTree.putVal(22);
        binarySearchTree.putVal(233);
        binarySearchTree.putVal(-200);

        binarySearchTree.removeMin();
    }

    @Test
    public void removeMax() throws Exception {
        BinarySearchTree1 binarySearchTree = new BinarySearchTree1();
        binarySearchTree.putVal(12);
        binarySearchTree.putVal(15);
        binarySearchTree.putVal(224);
        binarySearchTree.putVal(-348);
        binarySearchTree.putVal(22);
        binarySearchTree.putVal(233);
        binarySearchTree.putVal(-200);
        binarySearchTree.putVal(228);

        binarySearchTree.removeMax();
    }

    @Test
    public void floor() throws Exception {
        BinarySearchTree1<Integer> bst = new BinarySearchTree1();
        bst.putVal(18);
        bst.putVal(6);
        bst.putVal(21);
        bst.putVal(5);
        bst.putVal(9);
        bst.putVal(8);
        bst.putVal(10);
        bst.putVal(22);
        bst.putVal(16);
        bst.putVal(13);

        Integer floor = bst.floor(23);
        System.out.println(floor);
    }

    @Test
    public void ceiling() throws Exception {
        BinarySearchTree1<Integer> bst = new BinarySearchTree1();
        bst.putVal(18);
        bst.putVal(6);
        bst.putVal(21);
        bst.putVal(5);
        bst.putVal(9);
        bst.putVal(8);
        bst.putVal(10);
        bst.putVal(17);
        bst.putVal(20);
        bst.putVal(33);
        bst.putVal(29);

        Integer ceiling = bst.ceiling(11);
        System.out.println(ceiling);
    }

    @Test
    public void select() throws Exception {
        BinarySearchTree1<Integer> bst = new BinarySearchTree1();
        bst.putVal(18);
        bst.putVal(6);
        bst.putVal(21);
        bst.putVal(5);
        bst.putVal(9);
        bst.putVal(8);
        bst.putVal(10);
        bst.putVal(17);
        bst.putVal(20);
        bst.putVal(33);
        bst.putVal(29);

        Integer ceiling = bst.select(3);
        System.out.println(ceiling);
    }

}
